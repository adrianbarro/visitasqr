<?php
include_once('clases/visitasClass.php');
class acompanante{
    public $id;
    public $nombre;
    public $id_departamento;
}

class acompClass
{
     /* get departamentos */
     public function getAcomps($id)
     {
        try{
            
            $db = getDB();
    
            $stmt = $db->prepare("SELECT row_number() over (order by id nulls last) as id,id as idacomp, nombre, id_departamento from acompanante where id_departamento = :id");   
            $stmt->bindParam("id", $id,PDO::PARAM_INT) ;          
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_CLASS, "acompanante");
            return $data;
            $db = null;
            
          }catch(PDOException $e){
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }
     public function getAcomp($id)
     {
        try{
            
            $db = getDB();
    
            $stmt = $db->prepare("SELECT count(*) as total from acompanante where id_departamento in (select id_departamento from acompanante where id = :id)");     
            $stmt->bindParam("id", $id,PDO::PARAM_INT) ;        
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);
            return $data->total;
            $db = null;
            
          }catch(PDOException $e){
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }
     

  
}


?>