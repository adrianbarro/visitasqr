<?php
class departamento{
    public $id;
    public $departamento;
}

class departamentosClass
{
     /* get departamentos */
     public function getDepartamentos()
     {
        try{
            
            $db = getDB();
    
            $stmt = $db->prepare("SELECT * from departamentos");             
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_CLASS, "departamento");
            return $data;
            $db = null;
            
          }catch(PDOException $e){
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

  
}


?>