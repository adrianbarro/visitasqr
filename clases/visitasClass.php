<?php
class visitasClass
{
     /* Registrar visita */
     public function visitaRegistration($nombre,$apellidos,$empresa,$departamento,$acompanante,$numero_random,$motivo)
     {
          try{
            $db = getDB();

            $date = date("D M d, Y G:i");
          
            $stmt = $db->prepare("INSERT INTO visitas(nombre,apellidos,empresa,fecha_registro,numero_validacion,departamento_id,acompanante,motivo) VALUES (:nombre,:apellidos,:empresa,CURRENT_TIMESTAMP(0)::TIMESTAMP WITHOUT TIME ZONE,:numero_random,:departamento,:acompanante,:motivo)");  
            $stmt->bindParam("nombre", $nombre,PDO::PARAM_STR) ;
            $stmt->bindParam("apellidos", $apellidos,PDO::PARAM_STR) ;
            $stmt->bindParam("empresa", $empresa,PDO::PARAM_STR) ;
           
            $stmt->bindParam("numero_random", $numero_random,PDO::PARAM_STR) ;
            $stmt->bindParam("acompanante", $acompanante,PDO::PARAM_INT) ;
            $stmt->bindParam("departamento", $departamento,PDO::PARAM_STR) ;
            $stmt->bindParam("motivo", $motivo,PDO::PARAM_STR) ;
            $stmt->execute();
            $id=$db->lastInsertId();
            $db = null;    
            $_SESSION['id']=$id;

            return $id;

          } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

     //Confirmar visita*/
     public function visitaConfirmation($id)
     {
          try{
            $db = getDB();

            $date = date("D M d, Y G:i");
          
            $stmt = $db->prepare("UPDATE visitas SET fecha_confirmacion = CURRENT_TIMESTAMP(0)::TIMESTAMP WITHOUT TIME ZONE WHERE id = :id");  
           
            $stmt->bindParam("id", $id,PDO::PARAM_STR) ;
            $stmt->execute();
            $db = null;

            return true;

          } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

     public function visitaFin($id, $firma)
     {
          try{
            $db = getDB();

            $date = date("D M d, Y G:i");
          
            $stmt = $db->prepare("UPDATE visitas SET fecha_finalizacion = CURRENT_TIMESTAMP(0)::TIMESTAMP WITHOUT TIME ZONE, firma = :firma WHERE id = :id");  
        
            $stmt->bindParam("firma", $firma,PDO::PARAM_STR) ;
            $stmt->bindParam("id", $id,PDO::PARAM_STR) ;
            $stmt->execute();
            $db = null;

            return true;

          } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

     public function getNumeroValidacion($id){

      try{
        $db = getDB();

        $stmt = $db->prepare("SELECT id,nombre,apellidos,empresa,numero_validacion from visitas  WHERE id = :id");
            $stmt->bindParam("id", $id,PDO::PARAM_STR) ;
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_OBJ);
            $db = null;

            return $data;

      }catch(PDOException $e){
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
      }
     }
     
     /* Visita Details */
     public function visitaDetails($id)
     {
        try{
          $db = getDB();
          $stmt = $db->prepare("SELECT v.id,v.nombre,apellidos,empresa,fecha_confirmacion, fecha_finalizacion,d.departamento,ac.nombre as acompanante,firma,motivo  FROM visitas v 
                                 INNER JOIN departamentos d on (d.id = v.departamento_id)
                                 INNER JOIN acompanante ac on (ac.id = v.acompanante)  WHERE v.id=:id");  
          $stmt->bindParam("id", $id,PDO::PARAM_INT);
          $stmt->execute();
          $data = $stmt->fetch(PDO::FETCH_OBJ);
          return $data;
         }
         catch(PDOException $e) {
          echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

     public function visitaClock($id)
     {
        try{
          $db = getDB();
          $stmt = $db->prepare("SELECT fecha_confirmacion FROM visitas WHERE id=:id");  
          $stmt->bindParam("id", $id,PDO::PARAM_INT);
          $stmt->execute();
          $data = $stmt->fetch(PDO::FETCH_OBJ);
          return $data;
         }
         catch(PDOException $e) {
          echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

     /*QR Actual Details */
     public function qrDetails()
     {
        try{
          $db = getDB();
          $stmt = $db->prepare("SELECT id FROM qr_actual LIMIT 1");  
          $stmt->execute();
          $data = $stmt->fetch(PDO::FETCH_OBJ);
          return $data;
        }
        catch(PDOException $e) {
          echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

     /*Insertar ID qr actual*/
     public function qrInsert($id){
        try{
            $db = getDB();
          
            $stmt = $db->prepare("INSERT INTO qr_actual(id) VALUES (:id)");  
            $stmt->bindParam("id", $id,PDO::PARAM_STR) ;
            $stmt->execute();
            $db = null;

        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    }

     /*Eliminar ID qr que estaba antes*/
     public function qrDelete(){
        try{
            $db = getDB();
          
            $stmt = $db->prepare("DELETE FROM qr_actual");
            $stmt->execute();
            $db = null;
            return true;

        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
            
        }
    }

    /*Estado ocupado*/
    public function setUso($id, $uso){
      try{
        $db = getDB();
      
        $stmt = $db->prepare("UPDATE visitas SET uso = :uso WHERE id = :id");  
        $stmt->bindParam("uso", $uso,PDO::PARAM_STR) ;      
        $stmt->bindParam("id", $id,PDO::PARAM_STR) ;
        $stmt->execute();
        $db = null;

        return true;

      } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
      }
    }

    
     public function getUso(){
      try{
        $db = getDB();
      
        $stmt = $db->prepare("SELECT * from visitas where uso = TRUE");  
        $stmt->execute();
        $data = $stmt->rowCount();
        $db = null;

        if($data > 0)
          return true;
        else
          return false;

      } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
      }
    }


}
?>