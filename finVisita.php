<!DOCTYPE html>
        <html>
        <head>
            <title>RegisofAuth</title>
			<script language="javascript" src="js/jquery-1.7.2.min.js"></script>
			<script language="javascript" src="js/fancywebsocket.js?1.2"></script>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
            <link rel="stylesheet" href="style.css?2">
			
        </head>
        <header id="bannerForm"></header>
        <body>
        
        <div id="container" class="px-5">
          
            <h1 class="tittle"><b>FIN DE LA VISITA</b></H1>
            <h3 class="subtittle"><small>Hasta la proxima!</small></h3>

            <div class="alert alert-success mt-5" role="alert">
            Su visita ha sido finalizada, compruebe que sus datos aparecen correctamente en la pantalla.
            </div>
            

              
        </div>

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
            
        </body>
        <footer class="footerForm"><hr class="shadow hrForm"></hr><p class="copyForm">Regisof© 2020</p></footer>
        </html>