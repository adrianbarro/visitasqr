﻿<!DOCTYPE html>
        <html>
        <head>
            <title>RegisofAuth</title>
			<script language="javascript" src="js/jquery-1.7.2.min.js"></script>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
            <link rel="stylesheet" href="style.css?2.1">
            
			
        </head>
        <header id="bannerForm"></header>
        <body>
        
        <div id="container" class="px-5">
          
            <h1 class="tittle"><b>FINALIZAR VISITA</b></H1>
            
            <div class="alert alert-success mt-5" role="alert">
            Finaliza la visita para completar el registro.
            </div>
            <div class="d-flex justify-content-center mt-5">
               
                <h1   class="tittle" role="alert">
                Duración de la visita
                </h1>
            </div>
            <div class="d-flex justify-content-center mt-3">              
                <h1 class="clock" role="alert">
                00:00
                </h1>
            </div>
            <div class="d-flex justify-content-start pl-4 pr-4">   
            <h1>Inserte firma</h1>
</div >   
            <div class="d-flex justify-content-center pl-4 pr-4">      
            <canvas id="pizarra"></canvas>
            </div>
            
            <div class="mt-5">
                    <button type="button" class="shadow button btn btn-success btn-lg btn-block p-3 buttonText" name="signupSubmit" value="Registrar" id="fin_btn">FINALIZAR</button>
            </div> 
        </div>

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
            <script src="js/firma.js?2"> </script>
            <script src="js/finalizar.js?2.1"> </script>
            
            <script src="js/fancywebsocket.js?2"></script>
            
        </body>
        <footer class="footerForm"><hr class="shadow hrForm"></hr><p class="copyForm">Regisof© 2020</p></footer>
        </html>
