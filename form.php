<?php 
include_once("clases/conect.php");
include_once('clases/visitasClass.php');
$visitasClass = new visitasClass();
$qrDetails=$visitasClass->qrDetails();
$id_actual = $qrDetails->id;
$id_formulario = $_GET['id'];

$errorMsgReg='';

if($id_actual == $id_formulario){

    ?>

<!DOCTYPE html>
        <html>
        <head>
            <title>RegisofAuth</title>
            <script language="javascript" src="js/jquery-1.7.2.min.js"></script>
            <script>
                if(localStorage.getItem("registroid")){
        window.location.replace("https://fresondepalos.info:3535/SocketsQr/finalizar.php");
    }
            </script>
			<script language="javascript" src="js/fancywebsocket.js?2.1"></script>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
            <link rel="stylesheet" href="style.css?2">
			
        </head>
        <header id="bannerForm"></header>
        <body id="body">
        <p></p>
        <div id="container" class="px-5">
          
            <h1 class="tittle"><b>REGISTRAR VISITA</b></H1>
            <h3 class="subtittle"><small>Paso 1 - Registra tus datos</small></h3>
            
                <form class="mt-3">
                    <div class="form-group" method="post" action="" name="signup">
                        <label for="InputNombre" class="inputField"><b>Nombre:</b></label>
                        <input type="text" class="form-control form-control-lg inputTexto" name="nombreReg" id="nombreReg" autocomplete="off" aria-describedby="emailHelp" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="inputField"><b>Apelidos:</b></label>
                        <input type="text" name="apellidosReg" id="apellidosReg" autocomplete="off"  class="form-control form-control-lg inputTexto" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="inputField"><b>Empresa:</b></label>
                        <input type="text" name="empresaReg" id="empresaReg" autocomplete="off"  class="form-control form-control-lg inputTexto" >
                    </div>
                    <div >
                        <div class="float-left w-48 mb-3">
                            <div class="form-group">
                                <label class="inputField"><b>Departamento:</b></label>
                                <select class="custom-select  inputTexto" id="departamentoReg">
                                    <option value="-1" selected>Sin departamento</option> 
                                </select>
                               
                            </div>
                        </div>        
                        <div class="float-right w-48 mb-3">
                            <div class="form-group">
                                    <label class="inputField"><b>Acompañante:</b></label>
                                    <select class="custom-select  inputTexto" id="acompReg">
                                            <option value="-1" selected>Sin acompañante</option> 
                                    </select>
                            </div>
                        </div>
                               
                    </div>
                    <div class="form-group mb-5">
                        <label for="exampleInputPassword1" class="inputField"><b>Motivo:</b></label>
                        <input type="text" name="motivoReg" id="motivoReg" autocomplete="off"  class="form-control form-control-lg inputTexto" >
                    </div>
                    
                    <div class="mt-5">
                    <button type="button" class="shadow button btn btn-success btn-lg btn-block p-3 buttonText" name="signupSubmit" value="Registrar" id="register_btn">REGISTRAR</button>
                    </div>
                </form>
            
        </div>

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
            <script src="js/form.js?2.1"></script>
          
        </body>
        <footer class="footerForm"><hr class="shadow hrForm"></hr><p class="copyForm">Regisof© 2020</p></footer>
        </html>
    <?php
}
else{
    ?>

        <!DOCTYPE html>
        <!DOCTYPE html>
        <html>
        <head>
            <title>RegisofAuth</title>
			<script language="javascript" src="js/jquery-1.7.2.min.js"></script>
			<script language="javascript" src="js/fancywebsocket.js?2"></script>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
            <link rel="stylesheet" href="style.css?2">
			
        </head>
        <header id="bannerForm"></header>
        <body>
        
        <div id="container" class="px-5">
          
            <h1 class="tittle text-danger"><b>ERROR!</b></H1>

            <div class="alert alert-danger mt-5" role="alert">
           El codigo QR escaneado ya no es valido.Pruebe a escanear el codigo de nuevo. 
            </div>
            

              
        </div>

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
            
        </body>
        <footer class="footerForm"><hr class="shadow hrForm"></hr><p class="copyForm">Regisof© 2020</p></footer>
        </html>

    <?php
}

/*if (!empty($_POST['signupSubmit'])) 
{

	$nombre=$_POST['nombreReg'];
	$apellidos=$_POST['apellidosReg'];
    $empresa=$_POST['empresaReg'];

	if(strlen(trim($nombre))>0 && strlen(trim($apellidos))>0 && strlen(trim($empresa))>0) 
	{
        $visitasClass->visitaRegistration($nombre, $apellidos, $empresa);
        $ID_VISITA = $_SESSION['id'];

        $filename = "qr_visitas/" .$ID_VISITA.".png";
        $tamCodeQR = 10;
        $CorrectionError = "L"; //M Q (H mejor)

        QRcode::png($ID_VISITA, $filename, $CorrectionError, $tamCodeQR, 2);

        $url=BASE_URL.'visitaRegistrada.php?id='.$ID_VISITA;
        header("Location: $url");
    
	}
    else
    {
      $errorMsgReg="Los campos no pueden quedar vacios";
    }
}*/

?>