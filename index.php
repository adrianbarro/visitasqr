<?php
session_start();
if(isset($_SESSION['poUJPHBD_2'])){
include("clases/conect.php");
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link rel="stylesheet" href="style.css?2">
        <link href="js/toastr.css" rel="stylesheet">
       

        <title></title>
        
    </head>
    <header id="banner"></header>
    <body class="contenedor">

    <div class="d-flex">

    <div   class="text-success col-md-6 d-flex justify-content-center"><h5><b>1. Escanee el codigo QR</b></h5></div>
    <div   class="text-success col-md-6  d-flex justify-content-center"><h5><b>2. Espere a que aparezca su registro</b></h5></div>
    </div>

    <div class="alert alert-danger alert-time" role="alert">El tiempo se ha agotado, intentelo de nuevo.</div>
    <div class="alert alert-danger alert-val-error" role="alert">La visita no ha podido ser confirmada, numero de confirmacion incorrecto.</div>
    <div  id="resultado" class="col-md-6 d-flex justify-content-center"></div>
    <div id="div2"  class="col-md-6 "></div>
        

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="js/toastr.js"></script>
    <script src="js/reconnecting-websocket.js?2.1"></script>
        <script src="js/fancywebsocket.js?2"></script>
       
        <script src="js/script.js?2.1"></script>
    </body>
    <footer clas=""><hr class="shadow"></hr><p class="copy">Regisof© 2020</p></footer>
    </html>
<?php
}
else{
    header('Location: login.php');
    die();
}
?>