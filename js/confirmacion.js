
$(document).ready(function(){
    //comprobar campo de uso antes de comenzar
    getNumber();
   
});

var getUrlParams = function(){

    var url = window.location;
    
    return url.href.split("=")[1];

}

var getNumber = function(){

    var numero = getUrlParams();

    $.ajax({
        type: "POST",
        url: 'ajax3.php',
        data: "id="+numero,
        dataType:"html",
        success: function(data) 
        {
             
            var numero = JSON.parse(data)[0].numero.numero_validacion;
            var array_num = [ Math.floor((Math.random() * 100) + 1), Math.floor((Math.random() * 100) + 1),numero];
            var posiciones_usadas = [];
            var html = "";

            while(posiciones_usadas.length != array_num.length){
              
                var posicion = Math.floor(Math.random() * array_num.length);
                if($.inArray(posicion,posiciones_usadas) == -1){
                    html += "<button class='btn-number' onclick=onclickNumber("+array_num[posicion]+","+numero+")>"+ array_num[posicion] +"</button>";
                    posiciones_usadas.push(posicion);
                }      
            }
           
            $('.numero_group').html(html);
            send(data);

            
        }       
    });
}

var onclickNumber = function(num,numero){

        if(num == numero){
            $.ajax({
                type: "POST",
                url: 'ajax4.php',
                data: "id="+getUrlParams(),
                dataType:"html",
                success: function(data) 
                {
                    var JSONdata = JSON.parse(data);
                    
                    localStorage.setItem("registroid", JSONdata[0].datos.id);
                 
                    send(data);                                                 
                    window.location.replace("https://fresondepalos.info:3535/SocketsQr/formsuccess.php");
                }
                });
        }else{
            var data = [
                {
                  "tipo": 3
                }
              ];
              data = JSON.stringify(data);
            send(data); 
            window.location.replace("https://fresondepalos.info:3535/SocketsQr/formError.php");
        }
        
    
}