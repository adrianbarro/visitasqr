var jc = '';
var elemento = '';

var FancyWebSocket = function (url) {
	var callbacks = {};
	var ws_url = url;
	var conn;

	this.bind = function (event_name, callback) {
		callbacks[event_name] = callbacks[event_name] || [];
		callbacks[event_name].push(callback);
		return this;
	};

	this.send = function (event_name, event_data) {
		this.conn.send(event_data);
		return this;
	};

	this.connect = function () {
		if (typeof (MozWebSocket) == 'function')
			this.conn = new MozWebSocket(url);
		else
			console.log(url);
		this.conn = new WebSocket(url);

		this.conn.onmessage = function (evt) {
			dispatch('message', evt.data);
		};

		this.conn.onclose = function () { dispatch('close', null) }
		this.conn.onopen = function () { dispatch('open', null) }
	};

	this.disconnect = function () {
		this.conn.close();
	};

	var dispatch = function (event_name, message) {

		if (message == null || message == "")//aqui es donde se realiza toda la accion
		{

		}
		else {

			var JSONdata = JSON.parse(message); //parseo la informacion
			console.log(JSONdata);
			// var nombre = JSONdata[0].nombre;
			// var apellidos = JSONdata[0].apellidos;
			// var empresa = JSONdata[0].empresa;



			// var resultado = '<div class="mx-5 my-2 alert alert-primary" role="alert"><embed loop="false" src="beep.mp3" hidden="true" autoplay="true">\
			// Bienvenido <b>'+ nombre +' '+ apellidos + '</b>! Su visita ha sido registrada como empresa <b>' + empresa + '</b>!\
			// </div>';

			// $("#div2").html($("#div2").html() + resultado);
			// iniciar();

			switch (JSONdata[0].tipo)//que tipo de actualizacion vamos a hacer(un nuevo mensaje, solicitud de amistad nueva, etc )
			{
				case 1:
					actualiza_mensaje(message);
					break;
				case 2:
					actualiza_solicitud(message);
					break;
				case 3:
					finaliza_solicitud();
					break;
				case 4:
					fin_solicitud(message);
					break;

			}
			//aqui se ejecuta toda la accion
		}
	}
};
var Server;
function send(text) {
	Server.send('message', text);
}
$(document).ready(function () {
	Server = new FancyWebSocket('ws://regisof.no-ip.info:9393');
	Server.bind('open', function () {
	});
	Server.bind('close', function (data) {
	});
	Server.bind('message', function (payload) {
	});
	Server.connect();
});
function actualiza_mensaje(message) {
	var intervalo2 = '';
	var numero = JSON.parse(message)[0].numero.numero_validacion;
	jc = $.confirm({

		title: 'Confirmar Visita',
		// content: '<div class="alert-number">Seleccione en la pantalla de su dispositivo el siguiente numero para confirmar su visita.<div>' + '<div class="d-flex justify-content-center"><h1>' + numero + '</h1></div>' + '<div class="progress"><div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>' +
		// 	'</div>',
		content: '<div class="alert-number">Seleccione en la pantalla de su dispositivo el siguiente numero para confirmar su visita.<div>' + '<div class="d-flex justify-content-center"><h1 class="numero-validate"> ' + numero + '</h1> </div>' + '<br><br><br<div class="num">Esta ventana se cerrara automaticamente en <b>30</b> segundos</div>',
		theme: 'supervan',
		container:'p',
		columnClass: 'col-md-8 col-md-offset-2',
		buttons: {

			cancelAction: {
				isHidden: true,
				
				text: "Tiempo restante",
				action: function () {
					
					clearInterval(intervalo2);
					var html = ' <h1 class="tittle text-danger"><b>ERROR!</b></H1>\
					<div class="alert alert-danger mt-5" role="alert">\
				   	El tiempo se ha agotado, la visita no ha podido ser confirmada, vuelva a escanear el Qr e intentelo de nuevo.\
					</div>';
					$("#container").html(html);
					$(".alert-time").toggle("drop",300);	

					setTimeout(function () {
						
						console.log("cerrar");
						$(".alert-time").toggle("drop",300);
					}, 10000);
				}
			}
		},
		onOpenBefore: function(){
			
			//añadir campo de uso antes de comenzar
			console.log( JSON.parse(message)[0].numero.id);
			$.ajax({
				type: "POST",
				url: 'ajax8.php',
				data: "id=" + JSON.parse(message)[0].numero.id + "&uso=TRUE",
				dataType: "html",
				success: function (data) {
					
				}
			});
		},
		onContentReady: function () {
				
			var progress = 30
			var texto = this.buttons.cancelAction.text;
			var button = this.$$cancelAction;
		
			console.log(texto);
			intervalo2 = setInterval(function () {
				console.log(progress);
				progress = progress - 1;
				$(".num").html("Esta ventana se cerrara automaticamente en <b class='time-num'>"+progress+"</b> segundos");	
				// $(".progress-bar").attr("style", "width:calc(100% - " + progress + "%)!important")
				if(progress == 0 ){
					button.trigger("click");
				
				}
			}, 1000);
					
		
		},
		onDestroy: function (){
			
			clearInterval(intervalo2);
			$.ajax({
				type: "POST",
				url: 'ajax8.php',
				data: "id=" + JSON.parse(message)[0].numero.id + "&uso=FALSE",
				dataType: "html",
				success: function (data) {
				
				}
			});
		
			iniciar();
		}

	});



	/*var JSONdata = JSON.parse(message); //parseo la informacion
				var tipo = JSONdata[0].tipo;
				var mensaje = JSONdata[0].mensaje;
				var fecha = JSONdata[0].fecha;
				
				var contenidoDiv  = $("#"+tipo).html();
				var mensajehtml   = fecha+' : '+mensaje;
				
				$("#"+tipo).html(contenidoDiv+'0000111'+mensajehtml);*/
}
function actualiza_solicitud(message) {
	jc.close();
	var JSONdata = JSON.parse(message); //parseo la informacion
	console.log(JSONdata);
	var nombre = JSONdata[0].datos.nombre;
	var apellidos = JSONdata[0].datos.apellidos;
	var empresa = JSONdata[0].datos.empresa;
	var id_registro = JSONdata[0].datos.id;
	var options = {};
	var resultado;

	 resultado = '<div style="display:none;" class="registro mx-5 my-2 alert alert-success" role="alert"><embed loop="false" src="beep.mp3" hidden="true" autoplay="true">\
			 Bienvenido <b>'+ nombre + ' ' + apellidos + '</b>! Su visita ha sido registrada como empresa <b>' + empresa + '</b>!\
			 </div>';
	
			 $("#div2").html($("#div2").html() + resultado);
			 $($(".registro").eq(-1)).toggle("drop", options, 500);
			 
			 setTimeout(function() {
			
				$($(".registro").eq(-1)).toggle("drop", options, 500,function(){
					$(this).remove();
				});
			}, 10000 );
	// if ($(".registro").length == 5) {

	// 	for (var i = 0; i < $(".registro").length; i++) {
	// 		if (i == 0) {
	// 			$($(".registro").eq(0)).toggle("drop", options, 300);

	// 		}

	// 	};

	// 	setTimeout(function () {

	// 		$($(".registro").eq(0)).remove();
	// 		$(".ui-effects-placeholder").remove();
	// 		$($(".registro").eq(-1)).toggle("drop", options, 500);
	// 	}, 300);

	// 	// Callback function to bring a hidden box back

	// }
	// else {
	// 	$($(".registro").eq(-1)).toggle("drop", options, 500);
	// }
	
		
	  


}
function finaliza_solicitud() {
	jc.close();
	$(".alert-val-error").toggle("drop",300);			
					setTimeout(function () {
						
						console.log("cerrar");
						$(".alert-val-error").toggle("drop",300);
					}, 5000);
}
function fin_solicitud(message) {
	
	var JSONdata = JSON.parse(message); //parseo la informacion
	console.log(JSONdata);
	var nombre = JSONdata[0].nombre;
	var apellidos = JSONdata[0].apellidos;	
	var options = {};
	var resultado;
	

		resultado = '<div style="display:none" class="registro mx-5 my-2 alert alert-success" role="alert"><embed loop="false" src="beep.mp3" hidden="true" autoplay="true">\
		<b>'+ nombre + ' ' + apellidos + '</b>, su visita ha sido finalizada, hasta pronto!\
		</div>';
	

	$("#div2").html($("#div2").html() + resultado);
	$($(".registro").eq(-1)).toggle("drop", options, 500);
	
	setTimeout(function() {
   
	   $($(".registro").eq(-1)).toggle("drop", options, 500,function(){
		   $(this).remove();
	   });
   }, 10000 );

	// if ($(".registro").length == 5) {

	// 	for (var i = 0; i < $(".registro").length; i++) {
	// 		if (i == 0) {
	// 			$($(".registro").eq(0)).toggle("drop", options, 300);

	// 		}

	// 	};

	// 	setTimeout(function () {

	// 		$($(".registro").eq(0)).remove();
	// 		$(".ui-effects-placeholder").remove();
	// 		$($(".registro").eq(-1)).toggle("drop", options, 500);
	// 	}, 300);

	// 	// Callback function to bring a hidden box back

	// }
	// else {
	// 	$($(".registro").eq(-1)).toggle("drop", options, 500);
	// }
	iniciar();
	

}





