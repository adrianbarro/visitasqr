var clock;
var nombre;
var apellidos;
const b64toBlob = (b64Data, contentType = 'image/png', sliceSize = 512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }


    return byteArrays;

}

$(document).ready(function () {
    enabledButton();
    startClock();
    $("#fin_btn").on('click', function () {
        var id = localStorage.getItem("registroid");
       
        $.ajax({
            data: "id=" + id + "&firma=" + (miCanvas.toDataURL()).split("+").join("(/n)"),
            url: 'ajax6.php',
            type: 'post',
            beforeSend: function () {

            },
            success: function (response) {
                localStorage.removeItem("registroid");

                clearInterval(clock);
                $.confirm({

                    columnClass: 'col-md-10',
                    containerFluid: true,
                    type: 'green',
                    theme: 'material',
                    buttons:{
                        Cerrar:{
                            btnClass: 'btn-red',
                            action: function(){
                                
                                var data = [
                                    {
                                        "nombre": nombre,
                                        "apellidos": apellidos,
                                      "tipo": 4
                                    }
                                  ];
                                  data = JSON.stringify(data);
                                send(data); 
                                window.location.replace("https://fresondepalos.info:3535/SocketsQr/finVisita.php");
                               
                            }
                        }
                    },
                    content: function () {
                        var self = this;
                        return $.ajax({
                            url: 'ajax7.php',
                            data: "id=" + id,
                            dataType: 'json',
                            method: 'post',


                        }).done(function (response) {

                            var datos = response[0].datos;
                            var hora_fin = new Date((datos.fecha_finalizacion).replace(" ", "T") + "+00:00");
                            var hora_con = new Date((datos.fecha_confirmacion).replace(" ", "T") + "+00:00");
                            nombre = datos.nombre;
                            apellidos =  datos.apellidos;


                            self.setContent('<div class="texto-modal">Nombre: ' + nombre + '</div>');
                            self.setContentAppend('<div class="texto-modal">Apellidos: ' + apellidos + '</div>');
                            self.setContentAppend('<div class="texto-modal">Empresa: ' + datos.empresa + '</div>');
                            self.setContentAppend('<div class="texto-modal">Departamento: ' + datos.departamento + '</div>');
                            self.setContentAppend('<div class="texto-modal">Acompañante: ' + datos.acompanante + '</div>');
                            self.setContentAppend('<div class="texto-modal">Motivo: ' + datos.motivo + '</div>');
                            self.setContentAppend('<div class="texto-modal">Entrada: ' + calcularTime(hora_con.getTime()) + '</div>');
                            self.setContentAppend('<div class="texto-modal">Salida: ' + calcularTime(hora_fin.getTime()) + '</div>');
                            self.setContentAppend('<div class="texto-modal">Duracion: ' + calcularTime(hora_fin.getTime() - hora_con.getTime()) + '</div>');
                            self.setContentAppend('<div class="texto-modal"><br>Firma: </div><br>' + "<div class='firma'><img src='" + datos.firma + "'/></div>");



                            self.setTitle("<h1 class='tittle-modal mb-5 mt-3'>DETALLES VISITA</h1>");
                        }).fail(function () {
                            self.setContent('Something went wrong.');
                        });
                    }
                });
            }
        });

        // var image = new Image();
        // image.src = miCanvas.toDataURL();
        // $('body').html(image);
    });
});

var startClock = function () {
    var id = localStorage.getItem("registroid");
   
    $.ajax({
        data: "id=" + id,
        url: 'ajax5.php',
        type: 'post',
        beforeSend: function () {

        },
        success: function (response) {
            var json = JSON.parse(response);
            var hora = json[0].datos.fecha_confirmacion;
            var date = new Date(hora.replace(" ", "T") + "+02:00");        

            clock = setInterval(function () {
                var dateNow = new Date();              
                var result = calcularTime(dateNow.getTime() - date.getTime());
                $('.clock').html(result);
            }, 1000);


        }
    });
}

var calcularTime = function (duration) {

    var seconds = Math.floor((duration / 1000) % 60),
        minutes = Math.floor((duration / (1000 * 60)) % 60),
        hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
}