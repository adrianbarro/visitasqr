var miCanvas = document.querySelector('#pizarra');
let lineas = [];
let correccionX = 0;
let correccionY = 0;
let pintarLinea = false;

let posicion = miCanvas.getBoundingClientRect()
correccionX = posicion.x;
correccionY = posicion.y;

let nuevaPosicionX = 0;
let nuevaPosicionY = 0;
console.log( $("#pizarra").width())
miCanvas.width = $("#pizarra").width();
miCanvas.height = 300;
let ctx = miCanvas.getContext('2d')

//======================================================================
// FUNCIONES
//======================================================================

/**
 * Funcion que empieza a dibujar la linea
 */
function empezarDibujo() {
    pintarLinea = true;
    lineas.push([]);
   
};

/**
 * Funcion dibuja la linea
 */
function dibujarLinea(event) {

    event.preventDefault();
    if (pintarLinea) {
      
        // Estilos de linea
        ctx.lineJoin = ctx.lineCap = 'round';
        ctx.lineWidth = 10;
        // Color de la linea
        ctx.strokeStyle = '#000';
        // Marca el nuevo punto
        nuevaPosicionX = 0;
        nuevaPosicionY = 0;
        if (event.changedTouches == undefined) {
            // Versión ratón
            nuevaPosicionX = event.layerX;
            nuevaPosicionY = event.layerY;
        } else {
            // Versión touch, pantalla tactil
            nuevaPosicionX = event.changedTouches[0].pageX - correccionX;
            nuevaPosicionY = event.changedTouches[0].pageY - correccionY;
        }
        // Guarda la linea
        lineas[lineas.length - 1].push({
            x: nuevaPosicionX,
            y: nuevaPosicionY
        });
    //     for(var i = 0; i < lineas.length ; i++) {
    //         var actual = lineas[i];
            
    //             delete lineas[i];
    //         }
        
    // }
        // Redibuja todas las lineas guardadas
        ctx.beginPath();
        lineas.forEach(function (segmento) {
           
            if(!jQuery.isEmptyObject(segmento)){
            ctx.moveTo(segmento[0].x, segmento[0].y);
            segmento.forEach(function (punto, index) {
                ctx.lineTo(punto.x, punto.y);
            });
        }
        });
        ctx.stroke();
    }
}

/**
 * Funcion que deja de dibujar la linea
 */
function pararDibujar() {
   
    pintarLinea = false;
    var dataURL = miCanvas.toDataURL();
    console.log(dataURL);
  
    if (!$("#borrar").length) {
        $("#pizarra").parent().append('<img id="borrar" width="224" height="224" src="https://image.flaticon.com/icons/svg/845/845648.svg" class="loaded" onClick="borrarFirma()">');
        $("#borrar").css({
            top: $("#pizarra").offset().top - $("#borrar").width() / 2,
            right: $("#pizarra").offset().left - $("#borrar").width() / 2,
        })
        
    }
    enabledButton();
}

function borrarFirma() {

    ctx.clearRect(0, 0, miCanvas.width, miCanvas.height);
    $("#borrar").remove();
 lineas = [];
 enabledButton();

}
function enabledButton(){
    var enabled = false;
    lineas.forEach(function (segmento) {
           
        if(!jQuery.isEmptyObject(segmento)){
            enabled = true;
        };
   
    });

    if(enabled == true){
        console.log("true");
        $("#fin_btn").removeAttr("disabled");
    }else{
        $("#fin_btn").prop("disabled","true");
    }
}

//======================================================================
// EVENTOS
//======================================================================

// Eventos raton
miCanvas.addEventListener('mousedown', empezarDibujo, false);
miCanvas.addEventListener('mousemove', dibujarLinea, false);
miCanvas.addEventListener('mouseup', pararDibujar, false);

// Eventos pantallas táctiles
miCanvas.addEventListener('touchstart', empezarDibujo, false);
miCanvas.addEventListener('touchmove', dibujarLinea, false);
miCanvas.addEventListener('touchend', pararDibujar, false);
