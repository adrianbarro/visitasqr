var id_dept = -1;
var first_acomp = true;

$(document).ready(function () {
    
    loadDepartamentos();
    loadAcomp();
    $('#register_btn').on('click',function(){
        recargarQR();
    });
    $("#departamentoReg").on("change", function(){
        id_dept = $("#departamentoReg option:selected").val();
        $("#acompReg").html("<option value=-1 selected>Sin acompañante</option>");
        loadAcomp();
    });
    
});
var loadCampos = function(){
    $('#nombreReg').val(localStorage.getItem("nombreReg"));
    $('#apellidosReg').val(localStorage.getItem("apellidosReg"));
   $('#empresaReg').val(localStorage.getItem("empresaReg"));
   $('#departamentoReg option[value="'+localStorage.getItem("departamentoReg")+'"]').attr("selected", true);
    $('#motivoReg').val(localStorage.getItem("motivoReg"));
    $("#menu ").attr("selected", true);
};

var loadDepartamentos = function () {
    $.ajax({
        data: "option=1",
        url: 'ajax2.php',
        type: 'post',
        beforeSend: function () {

        },
        success: function (response) {
            var json = JSON.parse(response);
            var departamentos = json[0].datos;
            $.each(departamentos, function (key, value) {
                $("#departamentoReg").append("<option value=" + value.id + ">" + value.departamento + "</option>");
            });
            loadCampos();
           

        }
    });
};

var loadAcomp = function () {
  
    $.ajax({
       
        data: "option=2&id="+((first_acomp &&  (localStorage.getItem("departamentoReg") != null))?localStorage.getItem("departamentoReg") :id_dept),
        url: 'ajax2.php',
        type: 'post',
        beforeSend: function () {

        },
        success: function (response) {
            var json = JSON.parse(response);
            var departamentos = json[0].datos;
            $.each(departamentos, function (key, value) {
                $("#acompReg").append("<option value=" + value.idacomp + ">" + value.nombre + "</option>");
            });
            if (first_acomp){
            $('#acompReg option[value="'+localStorage.getItem("acompReg")+'"]').attr("selected", true);
            first_acomp = false;
            }
            

        }
    });
};

var recargarQR = function () {

    var nombre = document.getElementById('nombreReg').value;
    var apellidos = document.getElementById('apellidosReg').value;
    var empresa = document.getElementById('empresaReg').value;
    var departamento = $('#departamentoReg').val();
    var acompanante = $('#acompReg').val();
    var motivo = $('#motivoReg').val();
    var numero = Math.floor((Math.random() * 100) + 1);

    if (validation() == 0) {
        $.ajax({
            type: "POST",
            url: 'ajax2.php',
            data: "nombre=" + nombre + "&apellidos=" + apellidos + "&empresa=" + empresa + "&numero_random=" + numero + "&option=3&acompanante=" + acompanante + "&departamento=" + departamento + "&motivo=" + motivo,
            dataType: "html",
            success: function (data) {
              localStorage.setItem("nombreReg",nombre);
               localStorage.setItem("apellidosReg",apellidos);
              localStorage.setItem("empresaReg",empresa);
               localStorage.setItem("departamentoReg",departamento);
               localStorage.setItem("acompReg",acompanante);
                localStorage.setItem("motivoReg",motivo);
                window.location.replace("https://fresondepalos.info:3535/SocketsQr/confirmacion.php?id=" + JSON.parse(data)[0].id);
            }
        });
    }


   
}

var validation = function(){
    $('.input-red').removeClass('input-red');
    $('.texto-red').remove();
    var validado = 0;
    if( $('#nombreReg').val()==""){
        $('#nombreReg').addClass('input-red');
        $('#nombreReg').parent().append("<span class='texto-red'>* Campo requerido</span>");
       validado ++;
    }
    if( $('#apellidosReg').val()==""){
        $('#apellidosReg').addClass('input-red');
        $('#apellidosReg').parent().append("<span class='texto-red'>* Campo requerido</span>");
       validado ++;
    }
    if( $('#empresaReg').val()==""){
        $('#empresaReg').addClass('input-red');
        $('#empresaReg').parent().append("<span class='texto-red'>* Campo requerido</span>");
       validado ++;
    }
    if( $('#acompReg').val() == -1){
        $('#acompReg').addClass('input-red');
        $('#acompReg').parent().append("<span class='texto-red'>* Campo requerido</span>");
       validado ++;
    }
    if( $('#departamentoReg').val() == -1){
        $('#departamentoReg').addClass('input-red');
        $('#departamentoReg').parent().append("<span class='texto-red'>* Campo requerido</span>");
       validado ++;
    }
    if( $('#motivoReg').val() == ""){
        $('#motivoReg').addClass('input-red');
        $('#motivoReg').parent().append("<span class='texto-red'>* Campo requerido</span>");
       validado ++;
    }
    return validado;
}
