var intervalo;
var velocidad = 600000; //60'

$(document).ready(function(){
    
    realizaProceso();
    intervalo = setInterval(realizaProceso, velocidad);
})



function iniciar(){  
    realizaProceso();
    clearInterval(intervalo);
    intervalo = setInterval(realizaProceso, velocidad);
}

function realizaProceso() {
    $.ajax({
        data: null,
        url: 'ajax.php',
        type: 'post',
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor");
            console.log("buscando codigo");
        },
        success: function (response) {
            console.log("codigo encontrado");
            $("#resultado").html(response);
        }
    });

}